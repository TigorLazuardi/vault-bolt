interface BaseResult {
  request_id: string;
  lease_id: string;
  renewable: boolean;
  lease_duration: number;
  wrap_info: null;
  warnings: string[] | null;
}

export interface ListResult extends BaseResult {
  data: { keys: string[] };
  auth: null;
}

export interface CreateTokenResult<T = { [key: string]: string }> extends BaseResult {
  data: null;
  auth: {
    client_token: string;
    accessor: string;
    policies: string[];
    token_policies: string[];
    metadata: T;
    lease_duration: number;
    renewable: boolean;
    entity_id: string;
    token_type: 'service' | 'batch';
    orphan: boolean;
  };
}

export interface LookupTokenResult extends BaseResult {
  auth: null;
  data: {
    accessor: string;
    creation_time: number;
    creation_ttl: number;
    display_name: string;
    entity_id: string;
    expire_time: string;
    explicit_max_ttl: number;
    id: string;
    issue_time: string;
    meta: { [key: string]: string };
    num_uses: number;
    orphan: boolean;
    path: string;
    policies: string[];
    renewable: boolean;
    ttl: number;
    type: 'service' | 'batch';
  };
}
