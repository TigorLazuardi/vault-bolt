import moment from 'moment'

export default function formatDuration(seconds: number): string {
  if (seconds < 1) {
    return '_Expired_'
  }
  const result: (number | string)[] = []
  const a = moment.duration(seconds * 1000)

  const year = a.get('year')
  const month = a.get('month')
  const day = a.get('days')
  const hour = a.get('hours')
  const minute = a.get('minutes')

  year && result.push(year, year > 1 ? '_years_' : '_year_')
  month && result.push(month, month > 1 ? '_months_' : '_month_')
  day && result.push(day, day > 1 ? '_days_' : '_day_')
  hour && result.push(hour, hour > 1 ? '_hours_' : '_hour_')
  minute && result.push(minute, minute > 1 ? '_minutes_' : '_minute_')

  return result.join(' ')
}
