if (process.env.NODE_ENV === 'development') {
  require('dotenv').config()
}

import { App } from '@slack/bolt'
import { helloWorld } from './controllers/hello_world'
import { handleHome } from './controllers/home'
import { handleRequestToken } from './controllers/slash_request_token'

const signingSecret = process.env.SLACK_SIGNING_SECRET
const token = process.env.SLACK_BOT_TOKEN

if (!signingSecret || !token) {
  console.error('Singing secret and bot token is required')
  process.exit(1)
}

const port = process.env.PORT || 3000
const app = new App({ signingSecret, token })

async function close() {
  try {
    await app.stop()
    process.exit(0)
  } catch (e) {
    console.error(e)
    process.exit(1)
  }
}

process.on('SIGTERM', close)
process.on('SIGINT', close)

handleHome(app)
helloWorld(app)
handleRequestToken(app)

app.start(port).then(() => console.log('Bolt slack running on port', port))
