import axios from 'axios'
import cron from 'node-cron'

if (!process.env.VAULT_TOKEN) {
  console.error('Vault token required!')
  process.exit(1)
}

const vault = axios.create({
  baseURL: `${process.env.VAULT_HOST || 'http://127.0.0.1:8200'}/${
    process.env.VAULT_API_VERSON || 'v1'
  }`,
  timeout: 30000,
  headers: {
    'Content-Type': 'application/json',
    'X-Vault-Token': process.env.VAULT_TOKEN,
    'X-Vault-Request': 'true',
  },
})

// Set base token policy if not set yet
vault
  .get('/sys/policies/acl/bot-token-policy')
  .catch(() => {
    const policy = JSON.stringify(
      {
        path: {
          'auth/token/lookup-self': {
            capabilities: ['read'],
          },
          'auth/token/revoke-self': {
            capabilities: ['update'],
          },
          'sys/capabilities-self': {
            capabilities: ['update'],
          },
        },
      },
      null,
      '  ',
    )
    return vault.put('/sys/policies/acl/bot-token-policy', {
      policy,
    })
  })
  .catch((err) => {
    console.error(err)
    console.error('Failed to create `bot-token-policy`')
    process.exit(1)
  })

// Renew token
const schedule = process.env.VAULT_TOKEN_RENEW_SCHEDULE || '0 0 * * *'
cron.schedule(
  schedule,
  async () => {
    try {
      await vault.post('/auth/token/renew-self')
    } catch (e) {
      console.error(e)
    }
  },
  // @ts-ignore
  { timezone: process.env.VAULT_TOKEN_RENEW_TIMEZONE || 'Etc/UTC' },
)

export default vault
