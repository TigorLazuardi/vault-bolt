import { AxiosError } from 'axios'
import vault from '../infrastructures/vault'
import { CreateTokenResult } from '../models/vault'

export const engine = process.env.VAULT_SECRET_ENGINE

if (!engine) {
  console.error('Secret engine to manage must be specified')
  process.exit(1)
}

export async function createToken(spec: {
  config: string
  username: string
  policy: string
  ttl: string
  reason: string
}): Promise<string> {
  const { config, username, policy, ttl, reason } = spec
  if (!config) throw new Error('config name required')

  try {
    const policies = ['bot-token-policy']

    if (policy === 'read') {
      policies.push(`bot-read-${engine}-${config}`)
    } else if (policy === 'read-write') {
      policies.push(`bot-read-write-${engine}-${config}`)
    } else {
      throw new Error('invalid policy')
    }

    createPolicyIfNotExist(config, policy)

    const result = await vault.post<CreateTokenResult>('/auth/token/create', {
      role_name: username,
      meta: {
        requested: username,
        created_for: reason,
        created_at: new Date().toISOString(),
      },
      policies,
      type: 'batch',
      display_name: username,
      ttl,
      no_default_policy: true,
      renewable: false,
    })
    return result.data.auth.client_token
  } catch (e) {
    throw e
  }
}

export function createPolicyIfNotExist(config: string, policy: 'read' | 'read-write') {
  if (policy === 'read') {
    getReadPolicy(config)
      .catch(() => createReadPolicy(config))
      .catch((err) => {
        throw err
      })
  } else {
    getReadWritePolicy(config)
      .catch(() => createReadWritePolicy(config))
      .catch((err) => {
        throw err
      })
  }
}

async function getReadPolicy(config: string) {
  try {
    await vault.get(`/sys/policies/acl/bot-read-${engine}-${config}`)
  } catch (e) {
    throw e
  }
}

async function getReadWritePolicy(config: string) {
  try {
    await vault.get(`/sys/policies/acl/bot-read-write-${engine}-${config}`)
  } catch (e) {
    throw e
  }
}

function createReadPolicy(config: string) {
  const policy = {
    path: {
      [`${engine}/${config}`]: {
        capabilities: ['read'],
      },
      [`${engine}/`]: {
        capabilities: ['list'],
      },
    },
  }

  const strPolicy = JSON.stringify(policy, null, '  ')

  vault
    .put(`/sys/policies/acl/bot-read-${engine}-${config}`, {
      policy: strPolicy,
    })
    .catch((err: AxiosError) => {
      throw err
    })
}

function createReadWritePolicy(config: string) {
  const policy = {
    path: {
      [`${engine}/${config}`]: {
        capabilities: ['create', 'read', 'update', 'delete'],
      },
      [`${engine}/`]: {
        capabilities: ['list'],
      },
    },
  }

  const strPolicy = JSON.stringify(policy, null, '  ')

  vault
    .put(`/sys/policies/acl/bot-read-write-${engine}-${config}`, {
      policy: strPolicy,
    })
    .catch((err: AxiosError) => {
      throw err
    })
}
