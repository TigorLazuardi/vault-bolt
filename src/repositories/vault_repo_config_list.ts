import vault from '../infrastructures/vault'
import { ListResult } from '../models/vault'

const engine = process.env.VAULT_SECRET_ENGINE!

export async function getVaultConfigList(): Promise<string[]> {
  try {
    const result = await vault.get<ListResult>(`/${engine}?list=true`)
    return result.data.data.keys
  } catch (e) {
    throw e
  }
}
