import { AxiosError } from 'axios'
import vault from '../infrastructures/vault'
import { LookupTokenResult } from '../models/vault'

export async function lookupToken(token: string): Promise<LookupTokenResult> {
  try {
    const result = await vault.get<LookupTokenResult>(
      '/auth/token/lookup-self',
      {
        headers: { 'X-Vault-Token': token },
      },
    )
    return result.data
  } catch (e) {
    const err = <AxiosError>e
    if (err.code === '503') {
      throw new Error('Vault is sealed or in maintenance')
    }
    throw e
  }
}
