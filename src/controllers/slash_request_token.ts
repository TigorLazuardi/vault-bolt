import { App } from '@slack/bolt'
import formatDuration from '../helpers/parse_duration'
import { getVaultConfigList } from '../repositories/vault_repo_config_list'
import { createToken, engine } from '../repositories/vault_repo_create_token'
import { lookupToken } from '../repositories/vault_repo_lookup_token'

export function handleRequestToken(app: App) {
  app.command('/request_token', async ({ ack, say, body, context }) => {
    await ack()

    try {
      const secrets = await getVaultConfigList()
      if (!secrets.length) {
        await say("Sorry there's no config to list yet")
        return
      }

      const optionSecrets = secrets.map((secret) => ({
        text: {
          type: 'plain_text',
          text: secret,
          emoji: true,
        },
        value: secret,
      }))
      const modal = {
        type: 'modal',
        callback_id: 'modal_request_token',
        title: {
          type: 'plain_text',
          text: 'Request Token',
          emoji: true,
        },
        submit: {
          type: 'plain_text',
          text: 'Create Token',
          emoji: true,
        },
        close: {
          type: 'plain_text',
          text: 'Cancel',
          emoji: true,
        },
        blocks: [
          {
            type: 'input',
            block_id: 'config',
            element: {
              type: 'static_select',
              action_id: 'select_config',
              placeholder: {
                type: 'plain_text',
                text: 'Select an item',
                emoji: true,
              },
              initial_option: optionSecrets[0],
              options: optionSecrets,
            },
            label: {
              type: 'plain_text',
              text: 'Config',
              emoji: true,
            },
          },
          {
            type: 'input',
            block_id: 'duration',
            element: {
              type: 'static_select',
              action_id: 'select_duration',
              placeholder: {
                type: 'plain_text',
                text: 'Select an item',
                emoji: true,
              },
              initial_option: {
                text: {
                  type: 'plain_text',
                  text: 'One Hour',
                  emoji: true,
                },
                value: '1h',
              },
              options: [
                {
                  text: {
                    type: 'plain_text',
                    text: 'One Hour',
                    emoji: true,
                  },
                  value: '1h',
                },
                {
                  text: {
                    type: 'plain_text',
                    text: 'Two Hours',
                    emoji: true,
                  },
                  value: '2h',
                },
                {
                  text: {
                    type: 'plain_text',
                    text: 'Three Hours',
                    emoji: true,
                  },
                  value: '3h',
                },
                {
                  text: {
                    type: 'plain_text',
                    text: 'One Day',
                    emoji: true,
                  },
                  value: '24h',
                },
                {
                  text: {
                    type: 'plain_text',
                    text: 'Two Days',
                    emoji: true,
                  },
                  value: '48h',
                },
                {
                  text: {
                    type: 'plain_text',
                    text: 'Three Days',
                    emoji: true,
                  },
                  value: '72h',
                },
                {
                  text: {
                    type: 'plain_text',
                    text: 'Five Days',
                    emoji: true,
                  },
                  value: '120h',
                },
                {
                  text: {
                    type: 'plain_text',
                    text: 'One Week',
                    emoji: true,
                  },
                  value: '168h',
                },
                {
                  text: {
                    type: 'plain_text',
                    text: 'Two Weeks',
                    emoji: true,
                  },
                  value: '336h',
                },
                {
                  text: {
                    type: 'plain_text',
                    text: 'One Month',
                    emoji: true,
                  },
                  value: '768h',
                },
              ],
            },
            label: {
              type: 'plain_text',
              text: 'Duration',
              emoji: true,
            },
          },
          {
            type: 'input',
            block_id: 'policy',
            element: {
              type: 'static_select',
              action_id: 'select_policy',
              placeholder: {
                type: 'plain_text',
                text: 'Select an item',
                emoji: true,
              },
              initial_option: {
                text: {
                  type: 'plain_text',
                  text: 'Read',
                  emoji: true,
                },
                value: 'read',
              },
              options: [
                {
                  text: {
                    type: 'plain_text',
                    text: 'Read',
                    emoji: true,
                  },
                  value: 'read',
                },
                {
                  text: {
                    type: 'plain_text',
                    text: 'Read/Write',
                    emoji: true,
                  },
                  value: 'read-write',
                },
              ],
            },
            label: {
              type: 'plain_text',
              text: 'Access Policy',
              emoji: true,
            },
          },
          {
            type: 'input',
            block_id: 'reason',
            element: {
              type: 'plain_text_input',
              action_id: 'reason_value',
              multiline: false,
              placeholder: {
                type: 'plain_text',
                text: 'Short description why this token is created',
              },
            },
            label: {
              type: 'plain_text',
              text: 'Reason',
              emoji: true,
            },
          },
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text:
                '*TIP:*\n\nConsider using `One Hour` duration for quick fix.\n\nThe shorter the token life duration is, the less chance a breach could happen. Set duration as you need them, not any longer. You can always create new token if token lease is revoked.\n\nRevoke token using `/revoke` command when you are already done with it but the token still has remaining live duration.\n\n*_Do not_* share your token with anyone. If he/she wants access, let them create their own token, so their name will be recorded in the log as well.',
            },
          },
        ],
      }

      await app.client.views.open({
        token: context.botToken,
        trigger_id: body.trigger_id,
        // @ts-ignore
        view: modal,
      })
    } catch (e) {
      await say(e.message)
    }
  })

  app.view('modal_request_token', async ({ ack, body, view, context }) => {
    await ack()
    const config: string = view.state.values.config.select_config.selected_option.value
    const duration: string = view.state.values.duration.select_duration.selected_option.value
    const policy: string = view.state.values.policy.select_policy.selected_option.value
    const reason: string = view.state.values.reason.reason_value.value || ''

    try {
      const token = await createToken({
        config,
        ttl: duration,
        policy,
        username: body.user.name,
        reason,
      })
      const lookup = await lookupToken(token)
      const durLookup = formatDuration(lookup.data.ttl)
      await app.client.chat.postMessage({
        token: context.botToken,
        channel: body.user.id,
        text: `Your token is: \`\`\`${token}\`\`\``,
        blocks: [
          {
            type: 'section',
            text: {
              type: 'mrkdwn',
              text: `*Your token is:*\`\`\`${token}\`\`\``,
            },
          },
          {
            type: 'section',
            fields: [
              {
                type: 'mrkdwn',
                text: `*Created At:*\n<!date^${lookup.data.creation_time}^{date_long_pretty}, {time_secs}|${lookup.data.issue_time}>`,
              },
              {
                type: 'mrkdwn',
                text: `*Expired Date:*\n<!date^${
                  lookup.data.creation_time + lookup.data.creation_ttl
                }^{date_long_pretty}, {time_secs}|${lookup.data.expire_time}>`,
              },
              {
                type: 'mrkdwn',
                text: `*Duration:*\n${durLookup}`,
              },
              {
                type: 'mrkdwn',
                text: `*Config Path:*\n${engine}/${config}`,
              },
              {
                type: 'mrkdwn',
                text: `*Type:*\n${lookup.data.type === 'batch' ? 'Batch' : 'Service'}`,
              },
              {
                type: 'mrkdwn',
                text: `*Reason:*\n${lookup.data.meta?.created_for || '_Unmentioned_'}`,
              },
            ],
          },
        ],
      })
    } catch (e) {
      let message: string = e?.message || 'Unknown error'
      if (e?.code === '503') {
        message = 'Vault is sealed or in maintenance, please contact SysAdmin for access.'
      }
      await app.client.chat.postMessage({
        token: context.botToken,
        channel: body.user.id,
        text: message,
      })
    }
  })
}
