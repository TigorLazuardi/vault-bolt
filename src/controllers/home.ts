import { App } from '@slack/bolt';

export async function handleHome(b: App) {
  b.event('app_home_opened', async ({ event, context }) => {
    try {
      await b.client.views.publish({
        user_id: event.user,
        token: context.botToken,
        view: {
          type: 'home',
          callback_id: 'home_view',
          blocks: [
            {
              type: 'image',
              image_url: 'https://www.datocms-assets.com/2885/1508475795-vault-social-share.png',
              alt_text: 'hashicorp-vault',
            },
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text:
                  'Hello, this is Vault Service Home. To immediately start using the service, please go to `Messages` tab.',
              },
            },
            {
              type: 'divider',
            },
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text:
                  "*What is Vault Service Bot?*\n\n Vault Service is a bot to simplifies request to access to Vault and reduces burden to your SysAdmins so they don't have to wake up at night because you need access to some secret. Vault Service bot will scope your request to paths that you only requested, and also ask the time for the token TTL (Time to Live), so in case the token is stolen it will have no lasting damages or at least can be controlled.",
              },
            },
            {
              type: 'divider',
            },
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text:
                  "*Why?*\n\n Imagine the pain for you as the developer have to edit some secrets because at testing you found some secret is wrong. Because of security, you have to ask SysAdmin for new token, but that assumes your SysAdmin is available right now, between now and later is a wasted time. Then there's also the pain in being SysAdmin have to deal with token requests when they should have been dealing with deployment problems or something far more critical. This service bridges such inconveniences so both side can be happy while also having the security benefit of Vault.",
              },
            },
            {
              type: 'divider',
            },
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text:
                  '*Features* (Other than requesting token / revoke)\n\n\t1. Global Revoke in case a breach happened.\n\t2. Auto-Create Read/Write Policies to certain config paths.',
              },
            },
            {
              type: 'divider',
            },
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text:
                  '*Warning*\n\n All your actions on this Apps bot will be logged. Your user profile will be accessed and read when requesting or revoking access tokens.',
              },
            },
          ],
        },
      });
    } catch (e) {
      console.error(e);
    }
  });
}
