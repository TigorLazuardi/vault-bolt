FROM node:14.6-alpine3.12 AS runtime
WORKDIR /app
COPY tsconfig.json .
COPY package.json .
RUN npm install --production --silent
COPY src src/
CMD ["npm", "run", "start"]
